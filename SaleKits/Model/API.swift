
//  API.swift
//  SaleKits
//
//  Created by devsenior1 on 09/05/2022.


import Foundation
class API : Decodable{
    let code: String
    let message: String
    let warningCode: String
    let dataQA: [DataQA]

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case warningCode = "warningCode"
        case dataQA = "data"
    }

    required init(from decoder: Decoder) throws {

        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.code = try values.decodeIfPresent(String.self, forKey: .code) ?? ""
        self.message = try values.decodeIfPresent(String.self, forKey: .message) ?? ""
        self.warningCode = try values.decodeIfPresent(String.self, forKey: .warningCode) ?? ""
        self.dataQA = try values.decode([DataQA].self, forKey: .dataQA)
    }
//
//    func encode(to encoder: Encoder) throws {
//
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        try container.encode(code, forKey: .code)
//        try container.encode(message, forKey: .message)
//        try container.encode(warningCode, forKey: .warningCode)
//        try container.encode(dataA, forKey: .dataA)
//    }
}
