
import Foundation
import SwiftyJSON

struct FieldsModel {

    let code: String?
    let message: String?
    let warningCode: String?
    let data: [DataFields]?

    
    init(json: JSON) {
        self.code = json["code"].stringValue
        self.message = json["message"].stringValue
        self.warningCode = json["warningCode"].stringValue
        print("value map", json["data"])
        self.data = json["data"].arrayValue.map { DataFields(json: $0) }
    }

}

struct DataFields {

    let id: String
    let status: String
    let appType: String
    let appCode: String
    let appName: String
    let parentCode: Any
    let parentName: Any
    let extra: Any
    let sort: Any
    let createdBy: String
    let createdDate: String
    let lastModifiedBy: Any
    let lastModifiedDate: Any

    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.status = json["status"].stringValue
        self.appType = json["appType"].stringValue
        self.appCode = json["appCode"].stringValue
        self.appName = json["appName"].stringValue
        self.parentCode = json["parentCode"].stringValue
        self.parentName = json["parentName"].stringValue
        self.extra = json["extra"].stringValue
        self.sort = json["sort"].stringValue
        self.createdBy = json["createdBy"].stringValue
        self.createdDate = json["createdDate"].stringValue
        self.lastModifiedBy = json["lastModifiedBy"].stringValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
    }

}
