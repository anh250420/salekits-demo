//
//  Offerlistdata.swift
//  SaleKits
//
//  Created by devsenior1 on 16/05/2022.
//

import Foundation
import SwiftyJSON

struct Offerlistdata {

    let code: String?
    let message: String?
    let warningCode: String?
    let data: [DataOffer]?

    
    init(json: JSON) {
        self.code = json["code"].stringValue
        self.message = json["message"].stringValue
        self.warningCode = json["warningCode"].stringValue
        print("value map", json["data"])
        self.data = json["data"].arrayValue.map { DataOffer(json: $0) }
    }

}
struct DataOffer {

    var id: String?
    var status: String?
    var createdBy: String?
    var createdDate: String?
    var lastModifiedBy: String?
    var lastModifiedDate: String?
    var numberRowEffect: Int?
    var name: String?
    var type: String?
    var customerLevel: String?
    var fromDate: String?
    var toDate: String?
    var videoUrl: String?
    var supporter: String?
    var contentFileLink: String?
    var ruleFileLink: String?
    var imageLink: String?
    var detailImageLink: String?
    var code: String?
    var text: String?
    var productChecked: String?
    var productCheckedOld: String?
    var typeName: String?
    var customerLevelName: String?
    var productName: String?
    var attachmentDTOS: String?
    var resultDTOS: String?
    var createNewRecord: Bool?
    var updateRecord: Bool?

    init(json:JSON) {
        self.id = json["id"].stringValue
        self.status = json["status"].stringValue
        self.createdBy = json["createdBy"].stringValue
        self.createdDate = json["createdDate"].stringValue
        self.lastModifiedBy = json["lastModifiedBy"].stringValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
        self.numberRowEffect = json["numberRowEffect"].intValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
        self.name = json["name"].stringValue
        self.type = json["type"].stringValue
        self.customerLevel = json["customerLevel"].stringValue
        self.fromDate = json["fromDate"].stringValue
        self.toDate = json["toDate"].stringValue
        self.videoUrl = json["videoUrl"].stringValue
        self.supporter = json["supporter"].stringValue
        self.contentFileLink = json["contentFileLink"].stringValue
        self.ruleFileLink = json["ruleFileLink"].stringValue
        self.imageLink = json["imageLink"].stringValue
        self.detailImageLink = json["detailImageLink"].stringValue
        self.code = json["code"].stringValue
        self.text = json["text"].stringValue
        self.productChecked = json["productChecked"].stringValue
        self.productCheckedOld = json["productCheckedOld"].stringValue
        self.typeName = json["typeName"].stringValue
        self.customerLevelName = json["customerLevelName"].stringValue
        self.productName = json["productName"].stringValue
        self.attachmentDTOS = json["attachmentDTOS"].stringValue
        self.resultDTOS = json["resultDTOS"].stringValue
        self.createNewRecord = json["createNewRecord"].boolValue
        self.updateRecord = json["updateRecord"].boolValue
    }

}
