
import Foundation
import SwiftyJSON

struct RootClass {

    let code: String?
    let message: String?
    let warningCode: String?
    let data: DataB?

    
    init(json: JSON) {
        self.code = json["code"].stringValue
        self.message = json["message"].stringValue
        self.warningCode = json["warningCode"].stringValue
        print("value map", json["data"])
        self.data = DataB(json: json["data"])
    }
}


struct DataB {
 
    var id: String?
    var status: String?
    var createdBy: String?
    var createdDate: String?
    var lastModifiedBy: String?
    var lastModifiedDate: String?
    var numberRowEffect: Int?
    var name: String?
    var type: String?
    var customerLevel: String?
    var fromDate: String?
    var toDate: String?
    var videoUrl: String?
    var supporter: String?
    var contentFileLink: String?
    var ruleFileLink: String?
    var imageLink: String?
    var detailImageLink: String?
    var code: String?
    var text: String?
    var productChecked: String?
    var productCheckedOld: String?
    var typeName: String?
    var customerLevelName: String?
    var productName: String?
    var attachmentDTOS: [AttachmentDTOS]?
    var resultDTOS: [ResultDTOS]?
    var createNewRecord: Bool?
    var updateRecord: Bool?

 

    init(json:JSON) {
        self.id = json["id"].stringValue
        self.status = json["status"].stringValue
        self.createdBy = json["createdBy"].stringValue
        self.createdDate = json["createdDate"].stringValue
        self.lastModifiedBy = json["lastModifiedBy"].stringValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
        self.numberRowEffect = json["numberRowEffect"].intValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
        self.name = json["name"].stringValue
        self.type = json["type"].stringValue
        self.customerLevel = json["customerLevel"].stringValue
        self.fromDate = json["fromDate"].stringValue
        self.toDate = json["toDate"].stringValue
        self.videoUrl = json["videoUrl"].stringValue
        self.supporter = json["supporter"].stringValue
        self.contentFileLink = json["contentFileLink"].stringValue
        self.ruleFileLink = json["ruleFileLink"].stringValue
        self.imageLink = json["imageLink"].stringValue
        self.detailImageLink = json["detailImageLink"].stringValue
        self.code = json["code"].stringValue
        self.text = json["text"].stringValue
        self.productChecked = json["productChecked"].stringValue
        self.productCheckedOld = json["productCheckedOld"].stringValue
        self.typeName = json["typeName"].stringValue
        self.customerLevelName = json["customerLevelName"].stringValue
        self.productName = json["productName"].stringValue
        self.attachmentDTOS = json["attachmentDTOS"].arrayValue.map { AttachmentDTOS(json: $0) }
        self.resultDTOS = json["resultDTOS"].arrayValue.map { ResultDTOS(json: $0) }
        self.createNewRecord = json["createNewRecord"].boolValue
        self.updateRecord = json["updateRecord"].boolValue
    }

   
}


struct AttachmentDTOS: Decodable {

    let id: String?
    let status: String?
    let createdBy: String?
    let createdDate: String?
    let lastModifiedBy: String?
    let lastModifiedDate: String?
    let numberRowEffect: Int?
    let promotionsId: String?
    let name: String?
    let fileLink: String?
    let createNewRecord: Bool?
    let updateRecord: Bool?


    init(json:JSON) {
        self.id = json["id"].stringValue
        self.status = json["status"].stringValue
        self.createdBy = json["createdBy"].stringValue
        self.createdDate = json["createdDate"].stringValue
        self.lastModifiedBy = json["lastModifiedBy"].stringValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
        self.numberRowEffect = json["numberRowEffect"].intValue
        self.promotionsId = json["promotionsId"].stringValue
        self.name = json["name"].stringValue
        self.fileLink = json["fileLink"].stringValue
        self.createNewRecord = json["createNewRecord"].boolValue
        self.updateRecord = json["updateRecord"].boolValue
   }
}


struct ResultDTOS: Decodable {

    let id: String?
    let status: String?
    let createdBy: String?
    let createdDate: String?
    let lastModifiedBy: String?
    let lastModifiedDate: String?
    let numberRowEffect: Int?
    let promotionsId: String?
    let resultUpdate: String?
    let fileLink: String?
    let detailFileLink: String?
    let createNewRecord: Bool?
    let updateRecord: Bool?


    init(json:JSON) {
        self.id = json["id"].stringValue
        self.status = json["status"].stringValue
        self.createdBy = json["createdBy"].stringValue
        self.createdDate = json["createdDate"].stringValue
        self.lastModifiedBy = json["lastModifiedBy"].stringValue
        self.lastModifiedDate = json["lastModifiedDate"].stringValue
        self.numberRowEffect = json["numberRowEffect"].intValue
        self.promotionsId = json["promotionsId"].stringValue
        self.resultUpdate = json["resultUpdate"].stringValue
        self.fileLink = json["fileLink"].stringValue
        self.detailFileLink = json["detailFileLink"].stringValue
        self.createNewRecord = json["createNewRecord"].boolValue
        self.updateRecord = json["updateRecord"].boolValue
    }
}
