//
//  PiortizeViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 13/05/2022.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON


class PiortizeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    public var dataoffer: [DataOffer] = [DataOffer]()
    public var listdata: [DataOffer] = [DataOffer]()
   
    @IBOutlet weak var search: UISearchBar!
    var token:String = ""
    var field:String = ""
    var segment:String = ""
    @IBOutlet weak var pioritizeTbv: UITableView!
    @IBOutlet weak var fieldsTxt: DropDown!
    @IBOutlet weak var segmentTxt: DropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pioritizeTbv.register(UINib(nibName: "PiortizeTableViewCell", bundle: nil), forCellReuseIdentifier: "PiortizeTableViewCell")
        pioritizeTbv.dataSource = self
        pioritizeTbv.delegate = self
        fieldsTxt.checkMarkEnabled = false
        fieldsTxt.arrowSize = 10
        segmentTxt.checkMarkEnabled = false
        segmentTxt.arrowSize = 10
        search.delegate = self
        self.pioritizeTbv.reloadData()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callApiDataCustom()
        callApiDataFields()
        onClick()
    }
    func onClick(){
        fieldsTxt.didSelect{(selectedText , index ,id) in
            self.field = self.fieldsTxt.text ?? ""
            self.callApiDataOffer()
            self.pioritizeTbv.reloadData()
        }
        segmentTxt.didSelect{(selectedText , index ,id) in
            self.segment = self.segmentTxt.text ?? ""
            self.callApiDataOffer()
            self.pioritizeTbv.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataoffer.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = pioritizeTbv.dequeueReusableCell(withIdentifier:"PiortizeTableViewCell", for: indexPath) as! PiortizeTableViewCell
//        cell.piorImage.image = UIImage(named: "\(String(describing: dataoffer[indexPath.row].imageLink ?? ""))")
        cell.piorcellLB1.text = "\(String(describing: self.dataoffer[indexPath.row].productName ?? ""))"
        cell.piorcellLB2.text = "\(String(describing: self.dataoffer[indexPath.row].supporter ?? ""))"
        cell.piorcellLB3.text = "\(String(describing: self.dataoffer[indexPath.row].createdDate ?? ""))"
        return cell
    }

     @IBAction func Back(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
     }
    func callApiDataCustom() {
        let headers:HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json",
            "Content-Type": "application/json" ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/promotions/get-promotions-level", method: .get, parameters: [:], headers: headers).responseData
        { [weak self] (responseData) in
            let str = String(decoding: responseData.data ?? Data(), as: UTF8.self)
            
            print("abc123===", str)
            guard let strongSelf = self, let data = responseData.data else {
                return
            }
            do {
                let character = try JSON(data: data)
                print("\(character)>>>>>>>>>")
                print("abc123====", Customersegment(json: character))
                if let value = Customersegment(json: character).data {
                    var valueArray: [String] = []
                    for item in value {
                        valueArray.append(item.appCode ?? "")
                    }
                    strongSelf.segmentTxt.optionArray = valueArray
                }
            } catch {
                print("Error \(error)")
            }
        }
    }
    
    func callApiDataFields() {
        let headers:HTTPHeaders = [
                    "Authorization": "Bearer \(token)",
                    "Accept": "application/json",
                    "Content-Type": "application/json" ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/promotions/get-promotions-type", method: .get, parameters: [:], headers: headers).responseData
            { [weak self] (responseData) in
                let str = String(decoding: responseData.data ?? Data(), as: UTF8.self)

                print("abc123===", str)
                                guard let strongSelf = self, let data = responseData.data else {
                                    return
                                }
                                do {
                                    let character = try JSON(data: data)
                                    print("\(character)>>>>>>>>>")
                                    print("abc123====", FieldsModel(json: character))
                                    if let value = FieldsModel(json: character).data {
                                        var valueArray: [String] = []
                                        for item in value {
                                            valueArray.append(item.appCode )
                                        }
                                        strongSelf.fieldsTxt.optionArray = valueArray
                                    }
                                } catch {
                                    print("Error \(error)")
                                    
                                }
        }
}
    
    
    func callApiDataOffer() {
        let parameters: [String: Any] = [
            "name": "",
            "type": self.field,
            "customerLevel": self.segment
        ]
        let headers:HTTPHeaders = [
                    "Authorization": "Bearer \(token)",
                    "Accept": "application/json",
                    "Content-Type": "application/json" ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/promotions/get-promotions", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData
            { [weak self] (responseData) in
                let str = String(decoding: responseData.data ?? Data(), as: UTF8.self)

                print("abc123===", str)
                                guard let strongSelf = self, let data = responseData.data else {
                                    return
                                }
                                do {
                                    let character = try JSON(data: data)
                                    print("\(character)>>>>>>>>>")
                                    print("abc123====", Offerlistdata(json: character))
                                    if let value = Offerlistdata(json: character).data {
                                        strongSelf.dataoffer = value
                                        strongSelf.pioritizeTbv.reloadData()
                                        }
                                    print("Nguyễn Tuấn Anh - HUBT")
                                } catch {
                                    print("Error \(error)")
                                }
          }
      }
}
