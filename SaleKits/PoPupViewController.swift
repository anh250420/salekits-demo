//
//  PoPupViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 09/05/2022.
//

import UIKit
import iOSDropDown

class PoPupViewController: UIViewController {
    var itemSend:PioritizeModel = PioritizeModel(Number: 0)
    var listdata:[PioritizeModel] = [PioritizeModel]()
    @IBOutlet weak var dropdown1Txt: DropDown!
    @IBOutlet weak var dropdown2Txt: DropDown!
    @IBOutlet weak var dropdown3Txt: DropDown!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let option =  Pioritize()
        dropdown1Txt.optionArray = option.countries
        dropdown1Txt.optionIds = option.ids
        dropdown1Txt.checkMarkEnabled = false
        dropdown1Txt.arrowSize = 10
        dropdown2Txt.optionArray = option.color
        dropdown2Txt.optionIds = option.ids
        dropdown2Txt.checkMarkEnabled = false
        dropdown2Txt.arrowSize = 10
        dropdown3Txt.optionArray = option.countries
        dropdown3Txt.optionIds = option.ids
        dropdown3Txt.checkMarkEnabled = false
        dropdown3Txt.arrowSize = 10
        
    }
    

    @IBAction func savedataBtn(_ sender: Any) {
    }
    
    @IBAction func clodeBtn(_ sender: Any) {
        dismiss(animated: true)
    }
    
}
