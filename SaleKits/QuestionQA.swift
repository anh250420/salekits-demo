//
//  ChildViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftyJSON

class QuestionQA: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var childTableview: UITableView!
    @IBOutlet weak var searchChild: UISearchBar!
    
    public var dataAPI: [DataQA] = [DataQA]()
    public var listdata: [DataQA] = [DataQA]()
    var childNumber: String = ""
    var token:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        childTableview.register(UINib(nibName: "QASanTableViewCell", bundle: nil), forCellReuseIdentifier: "QASanTableViewCell")
        childTableview.dataSource = self
        childTableview.delegate = self
        searchChild.delegate = self
        searchChild.endEditing(true)
        searchChild.placeholder = "Search"
        searchChild.layer.borderWidth = 0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callApiData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

            searchBarSearchButtonClicked(searchBar)
        }
        
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }
        
        func searchBarSearchButtonClicked(_ SearchBar: UISearchBar){
            print("end searching --> Close Keyboard")
            listdata.removeAll()
            if SearchBar.text == "" {
                self.listdata = self.dataAPI
            }
            for item in dataAPI {
                   if item.feature1!.lowercased().contains(SearchBar.text!.lowercased()) {
                            listdata.append(item)
                   }
             }
             self.childTableview.reloadData()
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = childTableview.dequeueReusableCell(withIdentifier:"QASanTableViewCell", for: indexPath) as! QASanTableViewCell
        cell.productLB.text = "\(String(describing: self.listdata[indexPath.row].feature1 ?? ""))"
        cell.questionLB.text = "\(String(describing: self.listdata[indexPath.row].question ?? ""))"
        return cell
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }
    func callApiData() {
        let headers:HTTPHeaders = [
                    "Authorization": "Bearer \(token)",
                    "Accept": "application/json",
                    "Content-Type": "application/json" ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/qa/get-by-question?question=", method: .get, parameters: [:], headers: headers).response
            { [weak self] (responseData) in
                let str = String(decoding: responseData.data ?? Data(), as: UTF8.self)

                print("abc123===", str)
                                guard let strongSelf = self, let data = responseData.data else {
                                    return
                                }
                                do {
                                    let character = try JSONDecoder().decode(API.self, from: data)
                                    print("\(character)>>>>>>>>>")
                                    strongSelf.listdata = character.dataQA
                                    strongSelf.dataAPI = character.dataQA
                                        strongSelf.childTableview.reloadData()
                                } catch {
                                    print("Error \(error)")
            }
    }
}
}
