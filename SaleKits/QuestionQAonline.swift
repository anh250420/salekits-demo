//
//  Child2ViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 09/05/2022.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class QuestionQAonline: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var childTableview2: UITableView!
    public var dataAPI: [DataQA] = [DataQA]()
    var childNumber: String = ""
    var token:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        childTableview2.register(UINib(nibName: "QASanTableViewCell", bundle: nil), forCellReuseIdentifier: "QASanTableViewCell")
        childTableview2.dataSource = self
        childTableview2.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        callApiData()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "\(childNumber)")
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.dataAPI.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = childTableview2.dequeueReusableCell(withIdentifier:"QASanTableViewCell", for: indexPath) as! QASanTableViewCell
        cell.productLB.text = "\(String(describing: self.dataAPI[indexPath.row].feature1 ?? ""))"
        cell.questionLB.text = "\(String(describing: self.dataAPI[indexPath.row].question ?? ""))"
        return cell
    }
    @IBAction func nextPopup(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PoPupViewController") else { return }
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            present(vc, animated: true)
    }
    func callApiData() {
        let headers:HTTPHeaders = [
                    "Authorization": "Bearer \(token)",
                    "Accept": "application/json",
                    "Content-Type": "application/json" ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/qa/get-by-question?question=", method: .get, parameters: [:], headers: headers).response
            { [weak self] (responseData) in
                let str = String(decoding: responseData.data ?? Data(), as: UTF8.self)

                print("abc123===", str)
                                guard let strongSelf = self, let data = responseData.data else {
                                    return
                                }
                                do {
                                    let character = try JSONDecoder().decode(API.self, from: data)
                                    print("\(character)>>>>>>>>>")
                                    strongSelf.dataAPI = character.dataQA
                                        strongSelf.childTableview2.reloadData()
                                } catch {
                                    print("Error \(error)")
            }
    }
}
}
