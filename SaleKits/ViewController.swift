//
//  ViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    var username: String = "itsol_test01"
    var password: String = "123456a@"
    
    @IBOutlet weak var khdnBtn: UIButton!
    @IBOutlet weak var khcnBtn: UIButton!
    @IBOutlet weak var viewKHCN: UIView!
    @IBOutlet weak var vieweUser: UIView!
    @IBOutlet weak var viewUserin: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewPasswordin: UIView!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passWordTxt: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        khcnBtn.setImage(UIImage(named: "KHCND.png"), for: .normal)
        khdnBtn.setImage(UIImage(named: "KHDND.png"), for: .normal)
        viewKHCN.layer.cornerRadius = 10
        vieweUser.layer.cornerRadius = 10
        viewUserin.layer.cornerRadius = 10
        viewPassword.layer.cornerRadius = 10
        viewPasswordin.layer.cornerRadius = 10
        loginBtn.layer.cornerRadius = 10
    }
    
    func callApiLoginKHDN() {
        let parameters: [String: Any] = [
            "username" : username,
            "password" : password,
        ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/ldap-authenticate", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                            case .success(let value):
                                if let json = value as? [String: Any] {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
                                    vc.token = json["id_token"] as! String
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    print("value ==",json["id_token"])
                                } else {
                                    
                                }
                            case .failure(let error):
                                print(error)
                            }
                
            }
    }
    func callApiLoginKHCN() {
        let parameters: [String: Any] = [
            "username" : username,
            "password" : password,
        ]
        AF.request("http://salekit.khcn.api.myitsol.com/api/mobile/ldap-authenticate", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                            case .success(let value):
                                if let json = value as? [String: Any] {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "PiortizeViewController") as! PiortizeViewController
                                    vc.token = json["id_token"] as! String
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    print("value ==",json["id_token"])
                                } else {
                                    
                                }
                            case .failure(let error):
                                print(error)
                            }
            }
    }
   
    @IBAction func khdnButton(_ sender: Any) {
        khcnBtn.setImage(UIImage(named: "KHCN.png"), for: .normal)
        khdnBtn.setImage(UIImage(named: "KHDN.png"), for: .normal)
    }
    
    @IBAction func khcnButton(_ sender: Any) {
        khcnBtn.setImage(UIImage(named: "KHCND.png"), for: .normal)
        khdnBtn.setImage(UIImage(named: "KHDND.png"), for: .normal)
    }
    @IBAction func loginButton(_ sender: Any) {
            if usernameTxt.text == username, passWordTxt.text == password {
                if khcnBtn.currentImage == UIImage(named: "KHCND.png"){
                    callApiLoginKHDN()
                }else{
                    callApiLoginKHCN()
                }
            }else{
                let alert = UIAlertController(title: "Thông báo", message: "Thông tin đăng nhập không chính xác.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
            }
        
       }
}


