//
//  Man2ViewController.swift
//  SaleKits
//
//  Created by devsenior1 on 06/05/2022.
//

import UIKit
import XLPagerTabStrip

class ViewController2: ButtonBarPagerTabStripViewController {
    var token:String = ""
    override func viewDidLoad() {
        configureButtonBar()
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func configureButtonBar() {
//        settings.style.buttonBarBackgroundColor = UIColor(named: "#F8F9FE")
        settings.style.buttonBarItemBackgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9764705882, blue: 0.9960784314, alpha: 1)
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 18.0)!
        settings.style.buttonBarItemTitleColor = UIColor.black
        
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        settings.style.selectedBarHeight = 3.0
        settings.style.selectedBarBackgroundColor = .red
        
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
            newCell?.label.textColor = .red
        }
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child1 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QuestionQA") as! QuestionQA
        child1.token = token
        child1.childNumber = "Q&A có sẵn"
        
        let child2 = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QuestionQAonline") as! QuestionQAonline
        child2.childNumber = "Q&A Online"
        child2.token = token
        return [child1, child2]
    }
    
    @IBAction func backButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
