//
//  QASanTableViewCell.swift
//  SaleKits
//
//  Created by devsenior1 on 08/05/2022.
//

import UIKit

class QASanTableViewCell: UITableViewCell {

    @IBOutlet weak var ViewCell: UIView!
    @IBOutlet weak var productLB: UILabel!
    @IBOutlet weak var questionLB: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ViewCell.layer.cornerRadius = 10
        ViewCell.layer.shadowOpacity = 0.4
        ViewCell.layer.shadowColor = UIColor.gray.cgColor
        ViewCell.layer.shadowOffset = CGSize(width: 0, height: 0)
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
